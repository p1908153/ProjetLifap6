#ifndef MMAZE_DRAW_HPP
#define MMAZE_DRAW_HPP

#include "case.hpp"
#include "mur.hpp"
#include "site.hpp"
#include "couleurs.hpp"
#include "Tuile.hpp"

#include "UnionFind.hpp"

#include <string>
#include <vector>
#include <array>
#include <utility>
#include <random>
#include <map>
#include <string>


namespace MMaze {





class PadPlateau {
  public :

    enum struct TuileType 
    {
      DEPART = 0,
      CLASSIQUE = 1
    };

    //initialisation
    PadPlateau() ;
    ~PadPlateau();

    std::random_device rd;
    std::default_random_engine * rng;


    //ajout d'elements du plateau. Non reversible
    void ajouter_tuile(int newLine, int newColumn, int oldLine, int oldColumn) ;

    void ajouter_mur(int ligne, int colonne, Mur m) ;

    void ajouter_site(int ligne, int colonne, Case c, Site s) ;
    void ajouter_boutique(int ligne, int colonne, Case c) ;
    void ajouter_porte(int ligne, int colonne, Case c, Couleur couleur) ;
    void ajouter_objectif(int ligne, int colonne, Case c, Couleur couleur) ;
    void ajouter_sortie(int ligne, int colonne, Case c, Couleur couleur) ;
    void ajouter_vortex(int ligne, int colonne, Case c, Couleur couleur) ;

    void rotateTuile(int ligne, int colonne, int dir);

    void makeImpasse(int ligne, int colonne, std::vector<Case> & M);
    bool hasImpassLeft(int ligne, int colonne, std::vector<Case> & M, std::vector<Case> & merge);

    Site get_site(int ligne, int colonne, Case c);
    int get_indexMur(int ligne, int colonne, Mur m);
    void abattre_mur(int ligne, int colonne, Mur m) ;
    bool getWallState(int ligne, int colonne, Mur m);

    //placement des joueurs, modifiable par la suite
    void placer_joueur(int ligne, int colonne, Case c, Couleur couleur) ;

    void genererTuile(int ligne, int colonne, TuileType type, uint rotationValue);
    //void checkNeighbourDoor(std::vector<uint>& tab, int ligne, int colonne, int rotation);

    Tuile& getTuile(int x, int y);

    

    //export graphique
    
#ifndef NO_CAIRO
    //png ou svg
    //attention, il semble qu'il y ait parfois des bugs dans l'export svg
    void dessiner(const std::string& fichier, int unit = 64, float margin = 0.25) ;
#ifndef NO_IMAGICK
    std::string base64_png(int unit = 64, float margin = 0.25) ;
#endif
#endif

    //export console

    friend std::ostream& operator<<(std::ostream& out, const PadPlateau& plateau) ;

    //import / export json
    
    void save(const std::string& fichier) ;
    void load(const std::string& fichier) ;

  private :

    //tuiles actives
    std::vector<int> m_tuiles ;

    //murs sur les tuiles, 24 booleens par tuile
    std::vector<bool> m_murs ;

    //sites a ajouter sur les tuiles, 16 sites par tuile
    std::vector<Site> m_sites ;

    std::map<std::string, Tuile> tuileData;

    //joueurs
    //0 -> jaune
    //1 -> vert
    //2 -> orange
    //3 -> violet
    std::array<int, 8> m_tuiles_joueurs ;
    std::array<Case, 4> m_positions_joueurs ;
    std::array<bool, 4> m_presences_joueurs ;
} ;

std::ostream& operator<<(std::ostream& out, const PadPlateau& plateau) ;

} //end of namespace MMaze

#endif
