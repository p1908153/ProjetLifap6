#include "Djikstra.hpp"



Noeud::Noeud(int cIndex)
{
    noeudCase = MMaze::Case(cIndex);
}

Djikstra::Djikstra(int S, int L, int C, MMaze::PadPlateau * P)
{
    start = S;
    ligne = L;
    colonne = C;
    plateau = P;
    for(int i = 0; i < 16; i++)
    {
        if(plateau->get_site(ligne, colonne, MMaze::Case(i)) != MMaze::Site::BOUTIQUE) // On rajoute dans une map uniquement les noeuds des cases vides
        {
            mapNoeud.insert(std::pair<uint, Noeud*>(i, new Noeud(i))); 
        }
    }
    std::cout<<"Size = "<<mapNoeud.size()<<std::endl;
    makeGraphe(); // On genere le graphe
}

void Djikstra::makeGraphe()
{
    for(auto i = mapNoeud.begin(); i != mapNoeud.end(); i++) // On itere dans la map, et on ajoute les voisins de chaques noeuds
    {
        makeVoisin(i->second);
        //std::cout<<" i("<<i->first<<")->second->voisins.size() "<<i->second->voisins.size()<<std::endl;
    }
}

void Djikstra::makeVoisin(Noeud* noeud)
{
    MMaze::Case temp = noeud->noeudCase.droiteSafe(); // {droite, haut, bas, gauche}Safe -> renvoit 42 si case invalide
    uint l = 0; // On vérifie les cases dans la direction, tant qu'elles existent on les ajoute en voisins
    while(temp.index() != 42 && mapNoeud.find(temp.index()) != mapNoeud.end() && !plateau->getWallState(ligne, colonne, MMaze::Mur(temp.gauche(), temp)))
    {
        noeud->voisins.push_back({mapNoeud[temp.index()], DIRECTION::RIGHT, 1, l});
        temp = temp.droiteSafe();
        l++;
    }

    temp = noeud->noeudCase.gaucheSafe();
    l = 0;
    while(temp.index() != 42 && mapNoeud.find(temp.index()) != mapNoeud.end() && !plateau->getWallState(ligne, colonne, MMaze::Mur(temp.droite(), temp)))
    {
        noeud->voisins.push_back({mapNoeud[temp.index()], DIRECTION::LEFT, 1, l});
        temp = temp.gaucheSafe();
        l++;
    }

    l = 0;
    temp = noeud->noeudCase.hautSafe();
    
    while(temp.index() != 42 && mapNoeud.find(temp.index()) != mapNoeud.end() && !plateau->getWallState(ligne, colonne, MMaze::Mur(temp.bas(), temp)))
    {
        noeud->voisins.push_back({mapNoeud[temp.index()], DIRECTION::UP, 1, l});
        temp = temp.hautSafe();
        l++;
    }

    l=0;
    temp = noeud->noeudCase.basSafe();
    while(temp.index() != 42 && mapNoeud.find(temp.index()) != mapNoeud.end() && !plateau->getWallState(ligne, colonne, MMaze::Mur(temp.haut(), temp)))
    {
        noeud->voisins.push_back({mapNoeud[temp.index()], DIRECTION::DOWN, 1, l});
        temp = temp.basSafe();
        l++;
    }
}

std::vector<uint> Djikstra::calculateShortestPath(uint c1, uint c2)
{
    return calculateShortestPath(mapNoeud[c1], mapNoeud[c2]);
}

std::vector<uint> Djikstra::calculateShortestPath(Noeud * c1, Noeud * c2) // Parcour en largeur
{
    std::queue<Noeud*> queueNoeud; // On crée une queue avec les noeuds

    for(auto i = mapNoeud.begin(); i != mapNoeud.end(); i++) // On réinitialise les noeuds visité
    {
        i->second->visited = false;
    }

    
    queueNoeud.push(c1); // On ajoute le noeud du départ
    c1->visited = true; // On le dit visité pour éviter d'y retourner

    Noeud * curseur;
    while(!queueNoeud.empty()) // Tant que la queue n'est pas vide
    {
        curseur = queueNoeud.front(); // on Dequeue
        if(curseur == c2) break; // Si on est arrivé on s'arrete
        queueNoeud.pop(); // on supprime la valeur de queue
        for(int i = 0; i < curseur->voisins.size(); i++) // On parcour les voisins
        {
            if(!curseur->voisins[i].destination->visited) // On ajoute les non visité a la queue
            {
                curseur->voisins[i].destination->visited = true;
                curseur->voisins[i].destination->from = curseur;
                queueNoeud.push(curseur->voisins[i].destination);
            }
        }
    }

    curseur = c2;
    std::vector<uint> toAdd; // Vecteur pour le chemin

    while (curseur != c1) // Tant qu'on est pas arrivé au départ
    {
        toAdd.push_back(curseur->noeudCase.index()); // On ajoute l'index de la case
        curseur = curseur->from; // On remonte vers le noeud d'avant
    }

    toAdd.push_back(curseur->noeudCase.index()); // On ajoute le curseur en dernier

    std::reverse(toAdd.begin(), toAdd.end()); // On renverse le tableau pour l'avoir dans le bon sens

    return toAdd; // On renvoit le chemin
}

Djikstra::~Djikstra()
{
    for(auto i = mapNoeud.begin(); i != mapNoeud.end(); i++) // On itere dans la map, et on ajoute les voisins de chaques noeuds
    {
        delete i->second;
    }
}

