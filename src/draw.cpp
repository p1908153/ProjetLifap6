#include "draw.hpp"
#include "console_pad.hpp"
#include "json.hpp"

#include <vector>
#include <exception>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>

#define _USE_MATH_DEFINES
#include <cmath>

#ifndef NO_CAIRO
  #include <librsvg-2.0/librsvg/rsvg.h>
  #include <cairo/cairo.h>
  #include <cairo/cairo-svg.h>
  #include <Magick++.h>
#endif

namespace MMaze {

enum struct SiteAddition {
  JOUEUR = 100,
  JOUEUR_JAUNE = 101,
  JOUEUR_VERT = 102,
  JOUEUR_ORANGE = 103,
  JOUEUR_VIOLET = 104,

  MUR = 110
} ;


union drawable {
  drawable(Site s_in) : s(s_in) {}
  drawable(SiteAddition add_in) : add(add_in) {}

  Site s ;
  SiteAddition add ;
} ;

//traduction des sites en id dans le svg de style
static constexpr const char* site_names[] = {
  "background", "shop", "start", "door", "objective", "exit", "vortex", "", "", "", "player", "wall"
} ;

static constexpr const char* color_names[] = {
  "yellow", "green", "orange", "purple"
} ;

//traduction des sites en texte pour l'affichage console
static constexpr const char* site_symbols[] = {
  " ", "#", "", "" , "{$}", "[->", "(@)", "", "", "", "\\o/", ""
} ;

static std::size_t index_tuile(const std::vector<int>& t, int ligne, int colonne) {
  //on commence par la fin
  //ajouter des murs et des sites sur la derniere tuile ajoutee est efficace
  //la condition d'arret ci-dessous fonctionne car size_t est non signe
  //std::cout<<"Ligne = "<<ligne<<" Colonne = "<< colonne<<std::endl;
  for(std::size_t i = t.size() - 2; i < t.size(); i -= 2) {
    if(t[i] == ligne && t[i+1] == colonne) {
      return i/2 ;
    }
  }
  throw std::invalid_argument("aucune tuile a cette position") ;
}

void PadPlateau::ajouter_site(
    int ligne, int colonne, Case c, 
    Site site
    ) {
  //index de la tuile dans le tableau
  std::size_t index = index_tuile(m_tuiles, ligne, colonne) ;
  //placement du site
  m_sites[16 * index + c.index()] = site ;
}

PadPlateau::PadPlateau() : 
  m_positions_joueurs{Case(0), Case(0), Case(0), Case(0)}, m_presences_joueurs{false, false, false, false}
{
  rng = new std::default_random_engine(rd());
  //rng->seed(42);
}

PadPlateau::~PadPlateau()
{
  delete rng;
}

Site PadPlateau::get_site(int ligne, int colonne, Case c)
{
  std::size_t index = index_tuile(m_tuiles, ligne, colonne) ;
  //placement du site
  return m_sites[16 * index + c.index()];
}

bool PadPlateau::getWallState(int ligne, int colonne, Mur m)
{
  std::size_t index = index_tuile(m_tuiles, ligne, colonne) ;
  //placement du site
  return m_murs[24 * index + m.index()];
}

void PadPlateau::genererTuile(int ligne, int colonne, TuileType type, uint rotationValue)
{
  std::vector<Case> objectifs; // On stock les objectifs dans un vecteur
  Case portePos[4] = {Case(2), Case(4), Case(11), Case(13)}; // Position des portes
  Couleur couleur[4] = {Couleur::JAUNE, Couleur::VERT, Couleur::ORANGE, Couleur::VIOLET}; // Couleurs
  std::string key = std::to_string(ligne) + "_" + std::to_string(colonne); // Clé de la tuile
  for(int i = 0; i < 24; i++) // On remplit les murs
  {
    ajouter_mur(ligne, colonne, Mur(i));
  }
  if(type == TuileType::DEPART) // Si départ
  {
    Case pionPose[4] = {Case(5), Case(6), Case(9), Case(10)};
    std::shuffle(&couleur[0], &couleur[4], *rng);
    std::shuffle(&pionPose[0], &pionPose[4], *rng); // On randomise les pions et leurs couleurs

    for(int i = 0; i < 4; i++) // On ajoute les pions a leurs pos, on ajoute leurs porte, et on ajoute pion et porte dans le vecteur objectifs
    {
      ajouter_porte(ligne, colonne, portePos[i], couleur[i]);
      tuileData[key].Portes[couleur[i]] = portePos[i].index();
      placer_joueur(ligne, colonne, pionPose[i], couleur[i]);
      ajouter_site(ligne, colonne, pionPose[i], Site::DEPART);
      objectifs.push_back(portePos[i]);
      objectifs.push_back(pionPose[i]);
    } 
  }
  else if(type == TuileType::CLASSIQUE) // Si Classique
  {
    // On regarde les case autour et on rajoute une porte s'il faut la relier
    if(tuileData.find(std::to_string(ligne-1) + "_" + std::to_string(colonne)) != tuileData.end())
    {
      Site site = get_site(ligne-1, colonne, Case(13)); // Si la case relié de la cellule autour
      if(site >= Site::PORTE && site <= Site::PORTE_VIOLETTE) // est une porte
      {
        Case C = Case(2).tourne(rotationValue); // On tourne la case relié pour qu'elle corresponde
        objectifs.push_back(C); // On ajoute objectif / Porte
        ajouter_porte(ligne, colonne, C, Couleur::AUCUNE);
      }
    }
    if(tuileData.find(std::to_string(ligne) + "_" + std::to_string(colonne+1)) != tuileData.end())
    {
      Site site = get_site(ligne, colonne+1, Case(4));
      if(site >= Site::PORTE && site <= Site::PORTE_VIOLETTE) 
      {
        Case C = Case(11).tourne(rotationValue);
        objectifs.push_back(C);
        ajouter_porte(ligne, colonne, C, Couleur::AUCUNE);
      }
    }
    if(tuileData.find(std::to_string(ligne+1) + "_" + std::to_string(colonne)) != tuileData.end())
    {
      Site site = get_site(ligne+1, colonne, Case(2));
      if(site >= Site::PORTE && site <= Site::PORTE_VIOLETTE) 
      {
        Case C = Case(13).tourne(rotationValue);
        objectifs.push_back(C);
        ajouter_porte(ligne, colonne, C, Couleur::AUCUNE);
      }
    }
    if(tuileData.find(std::to_string(ligne) + "_" + std::to_string(colonne-1)) != tuileData.end())
    {
      Site site = get_site(ligne, colonne-1, Case(11));
      if(site >= Site::PORTE && site <= Site::PORTE_VIOLETTE) 
      {
        Case C = Case(4).tourne(rotationValue);
        objectifs.push_back(C);
        ajouter_porte(ligne, colonne, C, Couleur::AUCUNE);
      }
    }

    bool isExit = ((*rng)() % 2) == 1; // On regarde si c'est une sortie

    ajouter_porte(ligne, colonne, portePos[3], Couleur::AUCUNE); // On ajoute la porte en 13
    tuileData[key].Portes[Couleur::AUCUNE] = portePos[3].index(); // On la rajoute dans les data
    objectifs.push_back(portePos[3]); // On la rajoute dans le vecteur objectif

    int nbPorte = ((*rng)()%2)+1; // On calcul les porte a ajouter
    Couleur coul = couleur[(*rng)()%4];
    std::shuffle(&portePos[0], &portePos[3], *rng); // On mélange les porte

    for(int i = 0; i < nbPorte; i++) // On ajoute nbPorte
    {
      ajouter_porte(ligne, colonne, portePos[i], couleur[i]);
      tuileData[key].Portes[couleur[i]] = portePos[i].index();
      objectifs.push_back(portePos[i]);
    }

    
    Case c(0); // On randomise la Case c tant qu'elle n'est pas vide
    do
    {
      c = Case((*rng)()%16);
    } 
    while(get_site(ligne, colonne, c) != Site::AUCUN); 

    
    
    if(isExit) // Si c'est une sortie, on ajoute une sortie, sinon on ajoute un objectif
    {
      ajouter_sortie(ligne, colonne, c, coul); 
      tuileData[key].Sorties[coul] = c.index();
    }
    else 
    {
      ajouter_objectif(ligne, colonne, c, coul);
      tuileData[key].Sorties[coul] = c.index();
    }
    objectifs.push_back(c); // On ajoute l'objectifs
    
  }

  UnionFind murUnion({}); // On crée l'Union Find
  std::vector<MMaze::Mur> murTab; // On crée un vecteur de mur
  for(int i = 0; i < 16; i++) // On ajoute tout les murs dans murTab
  {
    if(i%4 != 3)
    {
      murTab.push_back(Mur(Case(i), Case(i).droite()));
    }

    if(i/4 < 3)
    {
      murTab.push_back(Mur(Case(i), Case(i).bas()));
    }
    
    murUnion.parent.push_back(i); // On ajoute leurs case a l'Union Find
    murUnion.hauteur.push_back(1); // On met leurs hauteurs a 1
    
  }

  shuffle(murTab.begin(), murTab.end(), *rng); // On shuffle les murs

  for(int i = 0; i < 24; i++)
  {
    if(murUnion.Find(murTab[i][0].index()) != murUnion.Find(murTab[i][1].index())) // On regarde si leurs parent sont sur le même ensemble
    {
      abattre_mur(ligne, colonne, murTab[i]); // Si non, on abat le mur
      murUnion.Union(murTab[i][0].index(), murTab[i][1].index()); // et on les rejoint
      //std::cout<<"Case0 : "<<murTab[i][0].index()<< " mur "<<murTab[i].index()<< " Case1 : "<<murTab[i][1].index()<< std::endl;
    }
    
    int equal = true; 
    int val = murUnion.parent.at(objectifs[0].index());
    for(int j = 1; j < (int)objectifs.size(); j++) // On check l'ensemble de chaque objectifs, et s'ils sont égaux on s'arrête
    {
      if(murUnion.parent.at(objectifs[j].index()) != val)
      {
        equal = false;
        break;
      }
    }
    if(equal) break;
  }
  bool impassLeft = true;
  std::vector<Case> toMerge; 
  
  while(impassLeft) // On calcul les impasses
  {
    std::vector<Case> tabMur;
    impassLeft = hasImpassLeft(ligne, colonne, tabMur, toMerge); // On calcul les impasse restante dans tabMur
    makeImpasse(ligne, colonne, tabMur); // On crée l'impasse
  }

  for(int i = 0; i < (int)toMerge.size(); i++) // On abat les murs internes des boutiques
  {
    if(toMerge[i].index() < 12 && get_site(ligne, colonne, toMerge[i].bas()) == Site::BOUTIQUE)
    {
      abattre_mur(ligne, colonne, Mur(toMerge[i], toMerge[i].bas()));
    }
    if(toMerge[i].index() > 3 && get_site(ligne, colonne, toMerge[i].haut()) == Site::BOUTIQUE)
    {
      abattre_mur(ligne, colonne, Mur(toMerge[i], toMerge[i].haut()));
    }
    if(toMerge[i].index()%4 != 0 && get_site(ligne, colonne, toMerge[i].gauche()) == Site::BOUTIQUE)
    {
      abattre_mur(ligne, colonne, Mur(toMerge[i], toMerge[i].gauche()));
    }
    if(toMerge[i].index()%4 != 3 && get_site(ligne, colonne, toMerge[i].droite()) == Site::BOUTIQUE)
    {
      abattre_mur(ligne, colonne, Mur(toMerge[i], toMerge[i].droite()));
    }
  }
}

void PadPlateau::makeImpasse(int ligne, int colonne, std::vector<Case> & M)
{
  for(int i = 0; i < (int)M.size(); i++) // On parcour la liste des impasse a crée
  {
    if(M.at(i).index() % 4 != 0 && !m_murs[get_indexMur(ligne, colonne, Mur(M.at(i), M.at(i).gauche()))]) // On ajoute des murs de chaques côtés
      ajouter_mur(ligne, colonne, Mur(M.at(i), M.at(i).gauche()));

    if(M.at(i).index() % 4 != 3 && !m_murs[get_indexMur(ligne, colonne, Mur(M.at(i), M.at(i).droite()))])
      ajouter_mur(ligne, colonne, Mur(M.at(i), M.at(i).droite()));

    if(M.at(i).index() / 4 != 0 && !m_murs[get_indexMur(ligne, colonne, Mur(M.at(i), M.at(i).haut()))])
      ajouter_mur(ligne, colonne, Mur(M.at(i), M.at(i).haut()));

    if(M.at(i).index() / 4 != 3 && !m_murs[get_indexMur(ligne, colonne, Mur(M.at(i), M.at(i).bas()))])
      ajouter_mur(ligne, colonne, Mur(M.at(i), M.at(i).bas()));

    ajouter_boutique(ligne, colonne, M.at(i)); // On ajoute la boutique
  }
}

bool PadPlateau::hasImpassLeft(int ligne, int colonne, std::vector<Case> & M, std::vector<Case> & merge)
{
  for(int i = 0; i < 16; i++) // On parcour toutes les cases, une impasse = 1 mur ouvert sur le nombre de mur max
  {
    if(get_site(ligne, colonne, Case(i)) == Site::AUCUN) // Si le site est vide
    {
      int murCount = 0; // On va compter le nombre de mur
      int maxMur = 4; // et le nombre de mur
      if(i % 4 != 0)  // On regarde les murs sur chaque côté
      {
        if(m_murs[get_indexMur(ligne, colonne, Mur(Case(i), Case(i).gauche()))]) murCount++; // Si il y en a un on incrémente mur count
      }
      else maxMur--; // S'ils n'existent pas on décrémente le nombre max de mur
  

      if(i % 4 != 3)
      {
        if(m_murs[get_indexMur(ligne, colonne, Mur(Case(i), Case(i).droite()))]) murCount++;
      }
      else maxMur--;

      if(i / 4 != 0)
      {
        if(m_murs[get_indexMur(ligne, colonne, Mur(Case(i), Case(i).haut()))]) murCount++;
      }
      else maxMur--;

      if(i / 4 != 3 )
      {
        if(m_murs[get_indexMur(ligne, colonne, Mur(Case(i), Case(i).bas()))]) murCount++;
      }
      else maxMur--;

      if(murCount == maxMur - 1) // si il y a 1 mur de dispo, on le met dans la liste a fusionner
      {
        M.push_back(Case(i));
        merge.push_back(Case(i));
      }
      else if(murCount == maxMur) // Si la case est fermer on ajoute une boutique
      {
        ajouter_boutique(ligne, colonne, Case(i));
        merge.push_back(Case(i));
      }
    }
  }
  return M.size() >= 1;
}

Tuile& PadPlateau::getTuile(int x, int y)
{
  return tuileData[std::to_string(x) + "_" + std::to_string(y)];
}

void PadPlateau::rotateTuile(int ligne, int colonne, int dir)
{
  Site tempCell[16]; // On stock les site dans un tableau temp
  bool tempWall[24]; // Même chose pour les murs 
  
  std::size_t index = index_tuile(m_tuiles, ligne, colonne) ; // On recuper l'index tuile
  std::string key = std::to_string(ligne) + "_" + std::to_string(colonne); // On clear les infos
  tuileData[key].Objectifs.clear();
  tuileData[key].Portes.clear();
  tuileData[key].Sorties.clear();
  for(int i = 0; i < 24; i++) // On tourne toutes les cases dans le temp
  {
    if(i < 16) tempCell[i] = m_sites[16 * index + Case(i).tourne(dir).index()];
    tempWall[i] = m_murs[24 * index + Mur(i).tourne(dir).index()];
  }

  for(int i = 0; i < 24; i++) // on remplace par la valeurs du temp
  {
    if(i < 16) 
    {
      m_sites[16 * index + i] = tempCell[i];
      switch(m_sites[16*index+i]) // On remplit les nouvelles infos
      {
        case Site::PORTE :
          tuileData[key].Portes[Couleur::AUCUNE] = i;
          break;
        
        case Site::PORTE_JAUNE:
          tuileData[key].Portes[Couleur::JAUNE] = i;
          break;

        case Site::PORTE_ORANGE:
          tuileData[key].Portes[Couleur::ORANGE] = i;
          break;

        case Site::PORTE_VERTE:
          tuileData[key].Portes[Couleur::VERT] = i;
          break;

        case Site::PORTE_VIOLETTE:
          tuileData[key].Portes[Couleur::VIOLET] = i;
          break;

        case Site::OBJECTIF_JAUNE:
          tuileData[key].Objectifs[Couleur::JAUNE] = i;
          break;

        case Site::OBJECTIF_ORANGE:
          tuileData[key].Objectifs[Couleur::ORANGE] = i;
          break;
        
        case Site::OBJECTIF_VERT:
          tuileData[key].Objectifs[Couleur::VERT] = i;
          break;

        case Site::OBJECTIF_VIOLET:
          tuileData[key].Objectifs[Couleur::VIOLET] = i;
          break;

        case Site::SORTIE_JAUNE:
          tuileData[key].Sorties[Couleur::JAUNE] = i;
          break;

        case Site::SORTIE_ORANGE:
          tuileData[key].Sorties[Couleur::ORANGE] = i;
          break;
        
        case Site::SORTIE_VERTE:
          tuileData[key].Sorties[Couleur::VERT] = i;
          break;

        case Site::SORTIE_VIOLETTE:
          tuileData[key].Sorties[Couleur::VIOLET] = i;
          break;
        default : break;
      }
    }
    m_murs[24 * index + i] = tempWall[i]; // On remplace les murs pas les temp
  }
}

void PadPlateau::ajouter_tuile(int newLine, int newColumn, int oldLine, int oldColumn) {
  //enregistrement de la position
  m_tuiles.push_back(newLine);
  m_tuiles.push_back(newColumn);
  std::string key = std::to_string(newLine) + "_" + std::to_string(newColumn); // On reucpere la clé de la Tuile
  tuileData[key] = Tuile(); // On crée la structure et on la store dans la map

  //allocation des murs et des sites, aucun mur et aucun site
  m_murs.resize(m_murs.size() + 24, false) ;
  m_sites.resize(m_sites.size() + 16, (Site) Site::AUCUN) ;
  int rotationValue = 0;
  if(newLine > oldLine) // On détermine la rotation
  {
    rotationValue = 2;
  }
  else if(newColumn < oldColumn)
  {
    rotationValue = 3;
  }
  else if(newColumn > oldColumn)
  {
    rotationValue = 1;
  }
  
  TuileType t;
  if(newLine == 4 && newColumn == 4) // on dtéermine le type de tuile (4_4 =  Tuile Depart, hardcoder)
    t = TuileType::DEPART;
  else
    t = TuileType::CLASSIQUE;

  genererTuile(newLine, newColumn, t, rotationValue); // On genere la tuile
  rotateTuile(newLine, newColumn, rotationValue); // On tourne la tuile
}

void PadPlateau::ajouter_mur(int ligne, int colonne, Mur m) {
  //index de la tuile dans le tableau
  std::size_t index = index_tuile(m_tuiles, ligne, colonne) ;
  //activation du mur
  m_murs[24 * index + m.index()] = true ;
}

int PadPlateau::get_indexMur(int ligne, int colonne, Mur m)
{
  std::size_t index = index_tuile(m_tuiles, ligne, colonne) ;
  //placement du site
  return 24 * index + m.index();
}

void PadPlateau::abattre_mur(int ligne, int colonne, Mur m) {
  //index de la tuile dans le tableau
  std::size_t index = index_tuile(m_tuiles, ligne, colonne) ;
  //activation du murq
  m_murs[24 * index + m.index()] = false ;
}

void PadPlateau::ajouter_boutique(int ligne, int colonne, Case c) {
  ajouter_site(ligne, colonne, c, Site::BOUTIQUE) ;
}

void PadPlateau::ajouter_porte(int ligne, int colonne, Case c, Couleur couleur) {
  //site colore a inserer
  Site site = colorer_site(Site::PORTE, couleur) ;
  //insertion
  ajouter_site(ligne, colonne, c, site) ;
}

void PadPlateau::ajouter_objectif(int ligne, int colonne, Case c, Couleur couleur) {
  //site colore a inserer
  Site site = colorer_site(Site::OBJECTIF, couleur) ;
  //insertion
  ajouter_site(ligne, colonne, c, site) ;
}

void PadPlateau::ajouter_sortie(int ligne, int colonne, Case c, Couleur couleur) {
  //site colore a inserer
  Site site = colorer_site(Site::SORTIE, couleur) ;
  //insertion
  ajouter_site(ligne, colonne, c, site) ;
}

void PadPlateau::ajouter_vortex(int ligne, int colonne, Case c, Couleur couleur) {
  //site colore a inserer
  Site site = colorer_site(Site::VORTEX, couleur) ;
  //insertion
  ajouter_site(ligne, colonne, c, site) ;
}

void PadPlateau::placer_joueur(int ligne, int colonne, Case c, Couleur couleur) {
  //index du joueur dans les tableaux
  int index_joueur = (int) couleur - 1 ;
  //activation du joueur si necessaire
  m_presences_joueurs[index_joueur] = true ;
  //tuile du joueur
  m_tuiles_joueurs[2*index_joueur] = ligne ;
  m_tuiles_joueurs[2*index_joueur + 1] = colonne ;
  //position dans la tuile
  m_positions_joueurs[index_joueur] = c ;
}

template<typename DessineElt>
static void dessiner_generique(
    const std::vector<int>& tuiles,
    const std::vector<bool>& murs,
    const std::vector<Site>& sites,
    const std::array<int, 8>& tuiles_joueurs,
    const std::array<Case, 4>& positions_joueurs,
    const std::array<bool, 4>& presences_joueurs,
    DessineElt& dessine_elt
    ) {
  //fond des tuiles
  for(unsigned int i = 0; i < tuiles.size(); i+= 2) {
    //parcours des 16 cases
    for(unsigned int ci = 0; ci < 16; ++ci) {
      //position pour dessiner
      Case c(ci) ;
      //fond ou boutique
      Site site = sites[8*i + ci] ;
      if(site == Site::BOUTIQUE) {
        //boutique
        dessine_elt(site, tuiles[i], tuiles[i+1], c, 0) ;
      } else {
        //fond
        dessine_elt(Site::AUCUN, tuiles[i], tuiles[i+1], c, 0) ;
      }
    }
  }
  //decor sur les tuiles
  for(unsigned int i = 0; i < tuiles.size(); i+= 2) {
    //parcours des 16 cases
    for(unsigned int ci = 0; ci < 16; ++ci) {
      //position pour dessiner
      Case c(ci) ;
      //site de la tuile
      Site site = sites[8*i + ci] ;
      //carracteristiques du site
      Site type = type_site(site) ;
      if(site != Site::AUCUN && site != Site::BOUTIQUE && type != Site::DEPART) {
        if(type == Site::PORTE) {
          try {
            //recherche d'une tuile voisine
            switch(ci) {
              case 2 :
                index_tuile(tuiles, tuiles[i]-1, tuiles[i+1]) ;
                break ;
              case 4 :
                index_tuile(tuiles, tuiles[i], tuiles[i+1]-1) ;
                break ;
              case 11 :
                index_tuile(tuiles, tuiles[i], tuiles[i+1]+1) ;
                break ;
              case 13 :
                index_tuile(tuiles, tuiles[i]+1, tuiles[i+1]) ;
                break ;
            }
            //si pas d'exception ici, pas de porte a afficher
          } catch(std::exception& a) {
            //une porte requiert une rotation selon sa case
            int rotation = 0 ;
            switch(ci) {
              case 2 :
                rotation = 1 ;
                break ;
              case 4 :
                rotation = 2 ;
                break ;
              case 13 :
                rotation = 3 ;
                break ;
            }
            dessine_elt(site, tuiles[i], tuiles[i+1], c, rotation) ;
          }
        } else {
          //site ne necessitant pas de rotation
          dessine_elt(site, tuiles[i], tuiles[i+1], c, 0) ;
        }
      }
      //bords
      if(type != Site::PORTE) {
        if(c.ligne() == 0) {
          dessine_elt(SiteAddition::MUR, tuiles[i], tuiles[i+1], c, 1) ;
        }
        if(c.ligne() == 3) {
          dessine_elt(SiteAddition::MUR, tuiles[i], tuiles[i+1], c, 3) ;
        }
        if(c.colonne() == 0) {
          dessine_elt(SiteAddition::MUR, tuiles[i], tuiles[i+1], c, 2) ;
        }
        if(c.colonne() == 3) {
          dessine_elt(SiteAddition::MUR, tuiles[i], tuiles[i+1], c, 0) ;
        }
      }
    }

    //murs
    for(unsigned int mi = 0; mi < 24; ++mi) {
      if(murs[12*i + mi]) {
        //un mur est pesent, position pour dessiner
        Mur m(mi) ;
        Case c = m[0] ;
        if(mi < 12) {
          //mur horizontal, rotation necessaire
          dessine_elt(SiteAddition::MUR, tuiles[i], tuiles[i+1], c, 3) ;
        } else {
          //mur vertical sans rotation
          dessine_elt(SiteAddition::MUR, tuiles[i], tuiles[i+1], c, 0) ;
        }
      }
    }
  }

  //dessin des joueurs
  
  for(unsigned int j = 0; j < 4; ++j) {
    if(presences_joueurs[j]) {
      dessine_elt(
          (SiteAddition) (((int) SiteAddition::JOUEUR) + j + 1), 
          tuiles_joueurs[2*j], 
          tuiles_joueurs[2*j+1], 
          positions_joueurs[j], 
          0
          ) ;
    }
  }
}

#ifndef NO_CAIRO

static double img_x(int ligne, int colonne, Case c) {
  return 4*colonne - ligne + (int) c.colonne() ;
}

static double img_y(int ligne, int colonne, Case c) {
  return 4*ligne + colonne + (int) c.ligne() ;
}

static void rotate(cairo_t* cr, double cx, double cy, int rotation) {
  cairo_translate(cr, cx + 0.5, cy + 0.5) ;
  cairo_rotate(cr, - rotation * M_PI/2) ;
  cairo_translate(cr, -cx - 0.5, -cy - 0.5) ;
}

static cairo_surface_t* dessiner_cairo(
    const std::string& style_file,
    const std::vector<int>& tuiles,
    const std::vector<bool>& murs,
    const std::vector<Site>& sites,
    const std::array<int, 8>& tuiles_joueurs,
    const std::array<Case, 4>& positions_joueurs,
    const std::array<bool, 4>& presences_joueurs
    ) {

  //chargement du fichier de style svg
  GError* svg_error = nullptr ;
  RsvgHandle* style = rsvg_handle_new_from_file(style_file.c_str(), &svg_error) ;
  if(svg_error) {
    throw std::invalid_argument("fichier de style introuvable") ;
  }

  //creation d'une surface cairo pour enregistrer le dessin
  cairo_surface_t* surface = cairo_recording_surface_create(CAIRO_CONTENT_COLOR_ALPHA, nullptr) ;

  //contexte de dessin
  cairo_t* cr = cairo_create(surface) ;

  //dessin d'site
  auto dessiner_site = [cr, style] (drawable elt, int tl, int tc, Case c, int rot) {
    //fenetre a dessiner
    double x = img_x(tl, tc, c) ;
    double y = img_y(tl, tc, c) ;
    RsvgRectangle rect({x, y, 1, 1}) ;

    //sauvegarde de la transformation initiale
    cairo_save(cr) ;

    //application de la rotation
    rotate(cr, x, y, rot) ;

    //id dans le fichier de style svg
    int site_index = (int) elt.s / 10 ;
    std::stringstream svg_id ;
    svg_id << "#" << site_names[site_index] ;
    if((int) elt.s % 10) {
      //le site a une couleur
      int site_color = (((int) elt.s % 10) - 1) % 4 ;
      svg_id << "_" << color_names[site_color] ;
    }

    //dessin
    //GError* error = nullptr ;
    rsvg_handle_render_layer(style, cr, svg_id.str().c_str(), &rect, /*&error*/ nullptr) ;
    //if(error) {
    //  std::cout << "got an error : " << svg_id.str() << " " << x << " " << y << " " << rot << std::endl ;
    //}

    //restauration de la transformation initiale
    cairo_restore(cr) ;
  } ;

  //dessin generique
  dessiner_generique(
        tuiles,
        murs,
        sites,
        tuiles_joueurs,
        positions_joueurs,
        presences_joueurs,
        dessiner_site
      ) ;

  cairo_surface_flush(surface) ;


  //finalisation
  cairo_destroy(cr) ;
  g_object_unref(style) ;

  return surface ;
}

void PadPlateau::dessiner(const std::string& fichier, int unit, float margin) {
  //dessin generique
  cairo_surface_t* record = dessiner_cairo(
      "mmaze.svg",
      m_tuiles,
      m_murs,
      m_sites,
      m_tuiles_joueurs,
      m_positions_joueurs,
      m_presences_joueurs
      ) ;

  double x0, y0, largeur, hauteur ;
  cairo_recording_surface_ink_extents(record, &x0, &y0, &largeur, &hauteur) ;

  //il semble qu'il y ait un bug dans le calcul des largeur, hauteur, et origine
  largeur -= 2 ;
  hauteur -= 2 ;
  x0 += 1 ;
  y0 += 1 ;

  //ouvrir une surface pour l'export
  int umargin = margin * unit ;
  cairo_surface_t* img ;
  const std::string& extension = fichier.substr(fichier.find_last_of(".") + 1) ;
  if(extension == "svg") {
     img = cairo_svg_surface_create(
         fichier.c_str(), unit*largeur + 2*umargin, unit*hauteur + 2*umargin
         ) ;
  } else if(extension == "png") {
    img = cairo_image_surface_create(
        CAIRO_FORMAT_ARGB32, unit*largeur + 2*umargin, unit*hauteur + 2*umargin
        ) ;
  } else {
    throw std::invalid_argument("seul l'export svg ou png est possible") ;
  }

  //creer un contexte pour l'export
  cairo_t* cr = cairo_create(img) ;
  cairo_translate(cr, umargin, umargin) ;
  cairo_scale(cr, unit, unit) ;

  //rejouer l'enregistrement
  cairo_set_source_surface(cr, record, -x0, -y0) ;
  cairo_paint(cr) ;
  cairo_show_page(cr) ;

  //finalisation de l'image
  cairo_surface_flush(img) ;

  //ecriture du png si besoin
  if(extension == "png") {
    cairo_surface_write_to_png(img, fichier.c_str()) ;
  }

  //destruction du contexte
  cairo_destroy(cr) ;

  //destruction de la surface d'export
  cairo_surface_finish(img) ;
  cairo_surface_destroy(img) ;
  cairo_surface_finish(record) ;
  cairo_surface_destroy(record) ;
}

#ifndef NO_IMAGICK

//encodage base 64 avec ImageMagick
static std::string base64_encode(const unsigned char* buf, unsigned int size) {
  Magick::Blob b(buf, size) ;
  return b.base64() ;
}

//recuperation des octets d'un png cairo dans un vector
static cairo_status_t render_base64(
    void* target, 
    const unsigned char* data,
    unsigned int length
    ) {
  std::vector<unsigned char>* output = (std::vector<unsigned char>*) target ;
  output->insert(output->end(), data, data + length) ;
  return CAIRO_STATUS_SUCCESS ;
}

std::string PadPlateau::base64_png(int unit, float margin) {
  //dessin generique
  cairo_surface_t* record = dessiner_cairo(
      "mmaze.svg",
      m_tuiles,
      m_murs,
      m_sites,
      m_tuiles_joueurs,
      m_positions_joueurs,
      m_presences_joueurs
      ) ;

  double x0, y0, largeur, hauteur ;
  cairo_recording_surface_ink_extents(record, &x0, &y0, &largeur, &hauteur) ;

  //il semble qu'il y ait un bug dans le calcul des largeur, hauteur, et origine
  largeur -= 2 ;
  hauteur -= 2 ;
  x0 += 1 ;
  y0 += 1 ;

  //ouvrir une surface pour l'export
  int umargin = margin * unit ;
  cairo_surface_t* img = cairo_image_surface_create(
      CAIRO_FORMAT_ARGB32, unit*largeur + 2*umargin, unit*hauteur + 2*umargin
      ) ;

  //creer un contexte pour l'export
  cairo_t* cr = cairo_create(img) ;
  cairo_translate(cr, umargin, umargin) ;
  cairo_scale(cr, unit, unit) ;

  //rejouer l'enregistrement
  cairo_set_source_surface(cr, record, -x0, -y0) ;
  cairo_paint(cr) ;

  //destruction du contexte
  cairo_destroy(cr) ;

  //finalisation de l'image
  cairo_surface_flush(img) ;

  //export des octets du png
  std::vector<unsigned char> octets ;
  cairo_surface_write_to_png_stream(
      img,
      render_base64,
      &octets
      ) ;

  //destruction de la surface d'export
  cairo_surface_finish(img) ;
  cairo_surface_destroy(img) ;
  cairo_surface_destroy(record) ;

  //encodage base 64
  return base64_encode(octets.data(), octets.size()) ;
}

#endif //NO_IMAGICK
#endif //NO_CAIRO

std::ostream& operator<<(std::ostream& out, const PadPlateau& plateau) {
  //pas de tuile pas de chocolat
  if(plateau.m_tuiles.size() == 0) return out ;

  //dimensions du plateau
  int lmin, lmax, cmin, cmax ;
  lmin = lmax = plateau.m_tuiles[0] ;
  cmin = cmax = plateau.m_tuiles[1] ;
  for(unsigned int i = 2; i < plateau.m_tuiles.size(); i+=2) {
    lmin = plateau.m_tuiles[i] < lmin ? plateau.m_tuiles[i] : lmin ;
    lmax = plateau.m_tuiles[i] > lmax ? plateau.m_tuiles[i] : lmax ;
    cmin = plateau.m_tuiles[i+1] < cmin ? plateau.m_tuiles[i+1] : cmin ;
    cmax = plateau.m_tuiles[i+1] > cmax ? plateau.m_tuiles[i+1] : cmax ;
  }

  //creation d'un buffer de suffisamment de lignes
  ConsolePad pad((lmax - lmin + 1) * 8 + 1 + 2 * (cmax - cmin)) ;

  //dessin d'site
  auto dessiner_site = [lmin, lmax, cmin, cmax, &pad] (drawable elt, int tl, int tc, Case ca, int rot) {
    //coordonnees tuile -> ligne et colonne dans le pad
    unsigned int labs = tl - lmin ;
    unsigned int cabs = tc - cmin ;

    //position de base de la case
    unsigned int l = 2 * ( 4 * labs + cabs + ca.ligne()) ;
    unsigned int c = 4 * ( 4 * cabs + (lmax - labs - lmin) + ca.colonne()) ;

    if((int) elt.s <= (int) Site::BOUTIQUE) {
      //dessin du fond
      const char* e = site_symbols[(int) elt.s / 10] ;
      pad.moveto(l, c) ;
      pad 
        << "+" << e << e << e << "+" << std::endl
        << e << e << e << e << e << std::endl
        << "+" << e << e << e << "+" ;
      return ;
    }

    if(elt.add == SiteAddition::MUR) {
      //dessin d'un mur
      switch(rot) {
        case 0:
          pad.moveto(l+1, c+4) ;
          pad << "|" ;
          break ;
        case 1:
          pad.moveto(l, c+1) ;
          pad << "---" ;
          break ;
        case 2:
          pad.moveto(l+1, c) ;
          pad << "|" ;
          break ;
        case 3:
          pad.moveto(l+2, c+1) ;
          pad << "---" ;
          break ;
      }
      return ;
    }

    //couleur du site s'il y en a une
    unsigned int site_color = (int) elt.s % 10 ;

    if((int) elt.s / 10 == (int) Site::PORTE / 10) {
      //dessin d'une porte
      switch(rot) {
        case 0:
          pad.moveto(l+1, c+4) ;
          pad << txt_colors[site_color] << ">" << TXT_CLEAR ;
          break ;
        case 1:
          pad.moveto(l, c+1) ;
          pad << txt_colors[site_color] << " ^ " << TXT_CLEAR ;
          break ;
        case 2:
          pad.moveto(l+1, c) ;
          pad << txt_colors[site_color] << "<" << TXT_CLEAR ;
          break ;
        case 3:
          pad.moveto(l+2, c+1) ;
          pad << txt_colors[site_color] << " v " << TXT_CLEAR ;
          break ;
      }
      return ;
    }

    //dessin du contenu d'une case
    pad.moveto(l+1, c+1) ;
    pad << txt_colors[site_color] << site_symbols[(int) elt.s / 10] << TXT_CLEAR ;
    
  } ;

  //dessin generique
  dessiner_generique(
        plateau.m_tuiles,
        plateau.m_murs,
        plateau.m_sites,
        plateau.m_tuiles_joueurs,
        plateau.m_positions_joueurs,
        plateau.m_presences_joueurs,
        dessiner_site
      ) ;

  out << pad.lines() ;
  return out ;
}

//export json

void PadPlateau::save(const std::string& fichier) {
  using json = nlohmann::json ;

  json bkp ;

  //tableau des tuiles
  bkp["tuiles"] = json::array() ;
  for(unsigned int i = 0; i < m_tuiles.size(); i+=2) {
    json tuile ;
    //tableau de deux entiers pour la position
    tuile["position"] = {m_tuiles[i], m_tuiles[i+1]} ;
    tuile["murs"] = json::array() ;
    //tableau de booleens pour les murs
    for(int m = 0; m < 24; ++m) {
      tuile["murs"].push_back((bool) m_murs[12*i + m]) ;
    }
    //tableau d'entiers pour les sites
    tuile["sites"] = json::array() ;
    for(int c = 0; c < 16; ++c) {
      tuile["sites"].push_back((int) m_sites[8*i + c]) ;
    }
    bkp["tuiles"].push_back(tuile) ;
  }

  //tableau des joueurs
  bkp["joueurs"] = json::array() ;
  for(unsigned int i = 0; i < 4; ++i) {
    if(m_presences_joueurs[i]) {
      json joueur ;
      //tableau de deux entiers pour la tuile du joueur
      joueur["tuile"] = {m_tuiles_joueurs[2*i], m_tuiles_joueurs[2*i+1]} ;
      //entier pour la case du joueur dans la tuile
      joueur["position"] = m_positions_joueurs[i].index() ;
      //couleur du joueur
      joueur["couleur"] = couleurs[i] ;
      bkp["joueurs"].push_back(joueur) ;
    }
  }

  std::ofstream f(fichier) ;
  f << std::setw(4) << bkp << std::endl ;
}

//import json

void PadPlateau::load(const std::string& fichier) {
  using json = nlohmann::json ;

  //reinitialisation
  m_tuiles.clear() ;
  m_murs.clear() ;
  m_sites.clear() ;
  m_presences_joueurs.fill(false) ;

  //chargement du json
  json input ;
  std::ifstream f(fichier) ;
  f >> input ;

  //recuperation des tuiles
  for(unsigned int i = 0; i < input["tuiles"].size(); ++i) {
    const json& tuile = input["tuiles"][i] ;
    //position de la tuile
    ajouter_tuile(tuile["position"][0], tuile["position"][1], tuile["position"][0], tuile["position"][1]) ;
    //murs
    for(int m = 0; m < 24; ++m) {
      m_murs[24*i + m] = tuile["murs"][m] ;
    }
    //sites
    for(int s = 0; s < 16; ++s) {
      m_sites[16*i + s] = (Site) tuile["sites"][s] ;
    }
  }

  //recuperation des joueurs
  for(unsigned int i = 0; i < input["joueurs"].size(); ++i) {
    const json& joueur = input["joueurs"][i] ;
    //index du joueur
    int index = (int) joueur["couleur"] - 1 ;
    m_presences_joueurs[index] = true ;
    m_positions_joueurs[index] = Case(joueur["position"]) ;
    m_tuiles_joueurs[2*index] = joueur["tuile"][0] ;
    m_tuiles_joueurs[2*index + 1] = joueur["tuile"][1] ;
  }

}

} //end of namespace MMaze
