#ifndef TUILE_HPP
#define TUILE_HPP

#include "couleurs.hpp"
#include "case.hpp"

#include <map>
#include <iostream>

enum struct TuileType 
{
    DEPART = 0,
    CLASSIQUE = 1
};



class Tuile
{
private:
    TuileType type;

public:
    Tuile();
    ~Tuile();
    void getInfo();
    std::map<MMaze::Couleur, int> Portes;
    std::map<MMaze::Couleur, int> Objectifs;
    std::map<MMaze::Couleur, int> Sorties;
};




#endif //TUILE_HPP