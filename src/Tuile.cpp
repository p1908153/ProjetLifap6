#include "Tuile.hpp"
Tuile::Tuile()
{

}

Tuile::~Tuile()
{
    
}

std::string colorToString(MMaze::Couleur col)
{
    switch (col)
    {
        case MMaze::Couleur::AUCUNE :  return "AUCUNE";
        case MMaze::Couleur::JAUNE :   return "JAUNE";
        case MMaze::Couleur::VERT :    return "VERT";
        case MMaze::Couleur::ORANGE :  return "ORANGE";
        case MMaze::Couleur::VIOLET :  return "VIOLET";
    
        default: return "ERREUR COULEUR INVALIDE";
    }
}

void Tuile::getInfo()
{
    std::cout<<"Infor de la case"<<std::endl;
    std::cout<<"Portes :: [";
    for (auto i = Portes.begin(); i != Portes.end(); i++)
    {
        std::cout << "{" + colorToString(i->first) +", "+ std::to_string(i->second)+"}, ";
    }
    std::cout<<"]"<<std::endl;

    std::cout<<"Objectifs :: [";
    for (auto i = Objectifs.begin(); i != Objectifs.end(); i++)
    {
        std::cout << "{" + colorToString(i->first) +", "+ std::to_string(i->second)+"}, ";
    }
    std::cout<<"]"<<std::endl;

    std::cout<<"Sorties :: [";
    for (auto i = Sorties.begin(); i != Sorties.end(); i++)
    {
        std::cout << "{" + colorToString(i->first) +", "+ std::to_string(i->second)+"}, ";
    }
    std::cout<<"]"<<std::endl;
}