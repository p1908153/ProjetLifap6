#include "melangeur.hpp"

namespace MMaze {
  namespace MelangeurOptions 
  {
    std::default_random_engine semeur ;
    std::random_device rd;

    void imprevisible() {
      semeur.seed(rd()) ;
    }

    void initialiser(std::size_t graine) {
      semeur.seed(graine) ;
    }

  }
}
