#include "Djikstra.hpp"

#include <iostream>

using namespace MMaze ;

int main() {
  PadPlateau pad ;
  int x, y, pX, pY, dir;
  bool exit = false;
  // On ajoute la tuile de départ
  x = y = 4;
  pad.ajouter_tuile(x, y, x, y);
  std::cout << pad << std::endl ;
  while(!exit)
  {
    // On selectionn une case
    std::cout<<"Case a selectionner, x puis y"<<std::endl;
    std::cin>>y>>x;
    // On demande la direction 
    std::cout<<"Direction de la nouvelle case, 0 : haut, 1: droite..."<<std::endl;
    std::cin>>dir;
    Case C(13); // On détermine la case de départ
    switch (dir)
    {
    case 0:
      pX = x-1; // on détermine la nouvelle cellule en fonction de la dir
      pY = y;
      break;
    case 1:
      pX = x;
      pY = y+1;
      C = C.tourne(3); // et on tourne le départ pour correspondre
      break;
    case 2:
      pX = x+1;
      pY = y;
      C = C.tourne(2);
      break;
    case 3:
      pX = x;
      pY = y-1;
      C = C.tourne(1);
      break;

    case 42 : // Code de sortie
      exit = true;
    
    default:
      pX = x;
      pY = y;
      break;
    }
    if(exit) break; // On arrête le programme
    pad.ajouter_tuile(pX, pY, x, y); // On genere la nouvelle tuile
    Djikstra parcour(C.index(), pX, pY, &pad); // On fait le graphe de la tuile
    Tuile t = pad.getTuile(pX, pY); // On recupere la structure d'info
    uint objectif = -1;
    if(t.Objectifs.size() > 0) // On détermine l'objectif a chercher
    {
      objectif = t.Objectifs.begin()->second;
    }
    else if(t.Sorties.size() > 0)
    {
      objectif = t.Sorties.begin()->second;
    }
    std::vector<uint> path = parcour.calculateShortestPath(C.index(), objectif); // On calcul le chemin
    
    
    std::cout << pad << std::endl ; // On affiche le plateau
    pad.getTuile(pX, pY).getInfo(); // on affiche les infos de la tuile

    std::cout<<"["; // On affiche le plus cours chemin jusqu'a l'objectif / sortie
    for(int i = 0; i < path.size(); i++)
    {
      std::cout<<path[i];
      if(i != path.size() - 1) std::cout<<", ";
      else std::cout<<"]"<<std::endl;
    }
  }


  pad.save("/tmp/pad.json") ;

  return 0 ;
}
