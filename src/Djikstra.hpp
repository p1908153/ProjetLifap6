#ifndef DJIKSTRA_HPP
#define DJIKSTRA_HPP

#include <vector>
#include <map>
#include <stdint.h>
#include <iostream>
#include <queue>
#include <algorithm>

#include "draw.hpp"

enum DIRECTION
{
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NOPE
};

struct Noeud;

struct Arrette
{
    Noeud * destination;
    DIRECTION dir;
    uint cout;
    uint longueur;
};

struct Noeud
{
    Noeud(int cIndex);
    MMaze::Case noeudCase;
    Noeud * from;
    std::vector<Arrette> voisins;
    bool visited = false;
};



class Djikstra
{
public:
    uint start;
    uint ligne;
    uint colonne;
    MMaze::PadPlateau * plateau;
    std::map<uint, Noeud *> mapNoeud;
    void makeGraphe();
    void makeVoisin(Noeud* voisins);
    std::vector<uint> calculateShortestPath(uint start, uint end);
    std::vector<uint> calculateShortestPath(Noeud * start, Noeud * end);

public:
    Djikstra(int S, int L, int C, MMaze::PadPlateau * P);
    ~Djikstra();
};





#endif //DJIKSTRA_HPP